<?php
namespace liuquanming;

class Sms
{
    private $account;
    private $password;

    public function __construct($account, $password)
    {
        $this->account = $account;
        $this->password = $password;
    }

    public function sendSms($phone,$content)
    {
        //发送短信逻辑
        $Mobile = $phone;
        $CorpID = $this->account;//用户名
        $Pwd = $this->password;//密码
        $Content = $content; //内容
        $ContentS = rawurlencode(mb_convert_encoding($Content, "gb2312", "utf-8"));//短信内容做GB2312转码处理
        $url = "http://sdk2.028lk.com/sdk2/LinkWS.asmx/BatchSend2?CorpID=".$CorpID."&Pwd=".$Pwd."&Mobile=".$Mobile."&Content=".$ContentS."&Cell=&SendTime=";
        $result=file_get_contents($url);
        $sendRes=simplexml_load_string($result);
        if ($sendRes) {
            return 1;
        } else {
            return 0;
        }
    }
}